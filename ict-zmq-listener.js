const zmq = require('zeromq')
const zmqSub = zmq.socket('sub')

//Initialize variables
const zmqPort = "5560"
const zmqNodeAddress = "tcp://localhost:" + zmqPort

//Currently not used
/*
const TRYTE_ALPHABET = "9ABCDEFGHIJKLMNOPQRSTUVWXYZ"

convertTrytesToAscii = (trytes) =>{
    //Check Trytes consistency
    if (typeof trytes !== 'string' || !new RegExp(`^[9A-Z]{1,}$`).test(trytes)) {
        throw new Error("Error: trytes are not consistent!")
    }
    //Check Trytes length
    /*
    if (trytes.length % 2) {
        throw new Error("Error: trytes length is not correct!")
    }

    let asciiText = ''

    for (let i = 0; i < trytes.length; i += 2) {
      asciiText += String.fromCharCode(TRYTE_ALPHABET.indexOf(trytes[i]) + TRYTE_ALPHABET.indexOf(trytes[i + 1]) * 27)
    }
    return asciiText
}
*/

//Open ZMQ communication channel with IXI node (ICT) and receive TX data
getDataFromZmq = () => {
  try {
    zmqSub.connect(zmqNodeAddress)
    //Subscribe to publication of specific messages
    console.log('Connected to ' + zmqNodeAddress);
    zmqSub.subscribe('tx')
    console.log('Subscribed to messages...');
    zmqSub.on('message',(msg) => { 
      let data = msg.toString().split('|')
      //console.log(data)
      console.log("hash: " + data[1])
      //console.log("signatureFragments: " + data[2])
      //console.log("address: " + data[3])
      //console.log("trytes: " + data[4])
      //console.log("isBundleHead: " + data[5])
      //console.log("timelockLowerBound: " + data[6])
      //console.log("timelockUpperBound: " + data[7])
      //console.log("attachmentTimestampLowerBound: " + data[8])
      //console.log("attachmentTimestamp: " + data[9])
      //console.log("attachmentTimestampUpperBound: " + data[10])
      //console.log("branchHash: " + data[11])
      //console.log("trunkHash: " + data[12])
      //console.log("essence: " + data[13])
      //console.log("extraDataDigest: " + data[14])
      //console.log("nonce: " + data[15])
      //console.log("tag: " + data[16])
      //console.log("value: " + data[17])
      console.log("decodedSignatureFragments: " + data[18]) //[I would prefer not to use it as, in case it contains "|" character, some problems may arise]
      console.log()
    })
  } catch (error) {
      console.log(error)
  }
}

//--------------------- START APPLICATION ---------------------
getDataFromZmq()
